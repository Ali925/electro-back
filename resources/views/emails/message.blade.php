<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
	<head>
    	<meta charset="utf-8" >
    	<title>{{$maintitle}}</title>
    </head>
<body>

<h4>Mesajı göndərənin adı - {{$mainname}}</h4> 
<h4>Mesajı göndərənin email adresi - {{$mainemail}}</h4> 
<h4>Mesajın mövzusu - {{$maintitle}}</h4> 

<h4>Mesajın mətni: </h4>

<p>{{$mainmessage}}</p>

</body>
</html>

