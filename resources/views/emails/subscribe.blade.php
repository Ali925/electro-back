<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
	<head>
    	<meta charset="utf-8" >
    	<title>{{$maintitle}}</title>
    </head>
<body>

<h1>{{$maintitle}}</h1>

<div class="image">
	<img src="{{$message->embed($mainimage)}}" width="600" alt="UNIMAX">
</div>

<div class="text">
	{!! $mainmessage !!}
</div>

</body>
</html>

