<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Products extends Model
{
	public function firms(){
        return $this->belongsTo('App\Firms', 'firm_id');
    }

    public function models(){
        return $this->belongsTo('App\Models', 'model_id');
    }

    public function types(){
        return $this->belongsTo('App\Types', 'type_id');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Colors');
    }

    public function reviews(){
        return $this->hasMany('App\Reviews', 'product_id');
    }

    public function carts(){
        return $this->hasMany('App\Carts', 'product_id');
    }

    public static function getList()
    {
        $result = [];
        $lists = Products::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
