<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Firms extends Model
{

    public function types(){
        return $this->hasMany('App\Types', 'firm_id');
    }

    public function products(){
        return $this->hasMany('App\Products', 'firm_id');
    }

    public function models()
    {
        return $this->belongsToMany('App\Models');
    }


    public static function getList()
    {
        $result = [];
        $lists = Firms::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
