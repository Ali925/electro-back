<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Types extends Model
{

	public function products(){
        return $this->hasMany('App\Products', 'type_id');
    }

	public function firms(){
        return $this->belongsTo('App\Firms', 'firm_id');
    }

    public function models(){
        return $this->belongsTo('App\Models', 'model_id');
    }
}
