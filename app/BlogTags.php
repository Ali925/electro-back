<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{

	public function blog_texts()
    {
        return $this->belongsToMany('App\BlogTexts');
    }



    public static function getList()
    {
        $result = [];
        $lists = BlogTags::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
