<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTexts extends Model
{

	public function blog_titles(){
        return $this->belongsTo('App\BlogTitles', 'blog_title_id');
    }

    public function blog_tags()
    {
        return $this->belongsToMany('App\BlogTags');
    }

    public function blog_comments(){
        return $this->hasMany('App\BlogComments', 'blog_id');
    }


    public static function getList()
    {
        $result = [];
        $lists = BlogTexts::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
