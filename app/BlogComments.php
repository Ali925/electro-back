<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComments extends Model
{
	public function customers(){
        return $this->belongsTo('App\Customers', 'customer_id');
    }

    public function blog_texts(){
        return $this->belongsTo('App\BlogTexts', 'blog_id');
    }

}
