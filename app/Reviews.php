<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{

	public function products(){
        return $this->belongsTo('App\Products', 'product_id');
    }

    public function customers(){
        return $this->belongsTo('App\Customers', 'customer_id');
    }

}
