<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Colors extends Model
{
	public function products()
    {
        return $this->belongsToMany('App\Products');
    }


    public static function getList()
    {
        $result = [];
        $lists = Colors::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
