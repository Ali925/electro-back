<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{

	public function customers(){
        return $this->belongsTo('App\Customers', 'customer_id');
    }

    public function products(){
        return $this->belongsTo('App\Products', 'product_id');
    }

}
