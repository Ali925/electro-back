<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Models extends Model
{

    public function types(){
        return $this->hasMany('App\Types', 'model_id');
    }

    public function products(){
        return $this->hasMany('App\Products', 'model_id');
    }

    public function firms()
    {
        return $this->belongsToMany('App\Firms');
    }



    public static function getList()
    {
        $result = [];
        $lists = Models::all();
        foreach ($lists as $list){ $result[$list->id] = $list->name; }
        return $result;
    }

}
