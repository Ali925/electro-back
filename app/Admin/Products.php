<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Products;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Products::class, function (ModelConfiguration $model){
    $model->setTitle('Məhsullar');

    Meta::addJS('jquery', asset('js/jquery-3.1.0.min.js'));
    Meta::addJS('select-colors', asset('js/select-colors.js'));
    Meta::addCSS('select-colors', asset('css/select-colors.css'));

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('firms.name', 'Brend'),
            AdminColumn::text('models.name', 'Məhsul növü'),
            AdminColumn::text('types.name', 'Model'),
            AdminColumn::text('price', 'Qiyməti'),
            AdminColumn::lists('colors.code','Rənglər'),
            AdminColumnEditable::checkbox('is_stock','Var', 'Yoxdu')->setLabel('Ehtiyyatda var'),
            AdminColumnEditable::checkbox('is_show','Görsənir', 'Görsənmir')->setLabel('Göstərilsin'),
            AdminColumnEditable::checkbox('is_discount','Bəli', 'Xeyir')->setLabel('Endirimdədir'),
            AdminColumn::text('new_price', 'Yeni qiyməti'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::select('firm_id', 'Brend')
                ->setModelForOptions(new \App\Firms())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name')->required(),
                AdminFormElement::select('model_id', 'Məhsul növü')
                ->setModelForOptions(new \App\Models())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name')->required(),
                AdminFormElement::select('type_id', 'Model')
                ->setModelForOptions(new \App\Types())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name'),
                AdminFormElement::number('price', 'Qiyməti')->required(),
                AdminFormElement::multiselect('colors','Rənglər')
                ->setModelForOptions(new \App\Colors())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('code','asc');
                })
                ->setDisplay('code')->required(),
                AdminFormElement::images('images', 'Şəkillər (şəkil yükləyərkən şəkilin adını böyük həriflərlə rənglər bölümündəki müvafiq rəng kodu kimi qeyd edin, misal FFFFFF.jpg)')
                ->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
                    $name = explode('.', $file->getClientOriginalName());
                    return 'images/uploads/' . $name[0];
                })
                ->storeAsJson()
                ->required(),
                AdminFormElement::checkbox('is_stock', 'Ehtiyyatda var'),
                AdminFormElement::checkbox('is_show', 'Göstərilsin'),
                AdminFormElement::checkbox('is_discount', 'Endirimdədir'),
                AdminFormElement::number('new_price', 'Yeni qiyməti'),
                AdminFormElement::text('network', 'Şəbəkə texnologiyaları'),
                AdminFormElement::text('twog', '2G'),
                AdminFormElement::text('threeg', '3G'),
                AdminFormElement::text('fourg', '4G'),
                AdminFormElement::text('network_speed', 'Şəbəkə sürəti'),
                AdminFormElement::text('date', 'Buraxılış tarixi'),
                AdminFormElement::text('size', 'Ölçülər'),
                AdminFormElement::text('weight', 'Çəki'),
                AdminFormElement::text('sims', 'SIM'),
                AdminFormElement::text('display_type', 'Ekran tipi'),
                AdminFormElement::text('display_inch', 'Ekran ölçüsü (düym)'),
                AdminFormElement::text('display_pixel', 'Ekran görüntü imkanı'),
                AdminFormElement::text('is_multitouch', 'Multitouch ekran'),
                AdminFormElement::text('display_defender', 'Ekran müdafiəsi'),
                AdminFormElement::text('sensor', 'Barmaq izi sensoru'),
                AdminFormElement::text('os', 'Əməliyyat sistemi'),
                AdminFormElement::text('chipset', 'Chipset'),
                AdminFormElement::text('cpu', 'CPU'),
                AdminFormElement::text('gpu', 'GPU'),
                AdminFormElement::text('ram', 'RAM'),
                AdminFormElement::text('memory', 'Daxili yaddaş'),
                AdminFormElement::text('memory_card', 'Genişləndirilə bilən yaddaş (kart)'),
                AdminFormElement::text('main_camera_pixel', 'Əsas kamera'),
                AdminFormElement::text('main_camera_desc', 'Əsas kamera xüsusiyyətləri'),
                AdminFormElement::text('video', 'Video'),
                AdminFormElement::text('second_camera', 'Ön kamera'),
                AdminFormElement::text('audio', 'Audio növləri'),
                AdminFormElement::text('vibration', 'Vibrasiya'),
                AdminFormElement::text('fm', 'Radio'),
                AdminFormElement::text('wlan', 'WLAN'),
                AdminFormElement::text('bluetooth', 'Bluetooth'),
                AdminFormElement::text('gps', 'GPS'),
                AdminFormElement::text('usb', 'USB'),
                AdminFormElement::text('messaging', 'Mesajlaşma'),
                AdminFormElement::text('batery_size', 'Batareya ölçüsü'),
                AdminFormElement::text('batery_first_length', 'Batareya danışıq vaxtı müddəti'),
                AdminFormElement::text('batery_second_length', 'Batareya musiqi çalanda müddəti'),
                AdminFormElement::text('other_features', 'Əlavə xüsusiyyətlər'),
                AdminFormElement::ckeditor('desc_text', 'Təsviri'),
                AdminFormElement::images('desc_images', 'Təsvir şəkilləri')->storeAsJson(),

        ]);

        return $form;
    });
});
