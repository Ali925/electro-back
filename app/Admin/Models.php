<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Models;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Models::class, function (ModelConfiguration $model){
    $model->setTitle('Məhsul növü');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required()->unique()

        ]);

        return $form;
    });
})->addMenuPage(Models::class, 1)->setIcon('fa fa-paperclip')->setAccessLogic(function (){
    return true;
});

