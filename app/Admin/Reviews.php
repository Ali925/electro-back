<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Reviews;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Reviews::class, function (ModelConfiguration $model){
    $model->setTitle('Rəylər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('review','Rəy'),
            AdminColumn::relatedLink('customers.name','İstifadəçi adı (qeydə alınmış)'),
            AdminColumn::text('username','İstifadəçi adı (qeydə alınmamış)'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('review','Rəy')->required(),
        ]);

        return $form;
    });
});

