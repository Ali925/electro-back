<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Banners;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Banners::class, function (ModelConfiguration $model){
    $model->setTitle('Bannerlər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('type','Banner tipi'),
            AdminColumn::text('link','Banner şəkili'),
            AdminColumn::text('image','Banner keçidi'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::select('type', 'Banner tipi', array('1' => 'Ana səhifə karusel', '2' => 'Ana səhifə mərkəz', '3' => 'Məhsullar səhifəsi sol tərəfdə'))->required(),
                AdminFormElement::image('link', 'Şəkil')->required(),
                AdminFormElement::text('image', "Keçid"),
                AdminFormElement::text('blue_text', "Göy başlıq (ana səhifə karusel)"),
                AdminFormElement::text('button', "Düymə (ana səhifə karusel)"),
                AdminFormElement::ckeditor('text', "Text (ana səhifə karusel)")
        ]);

        return $form;
    });
})->addMenuPage(Banners::class, 1)->setIcon('fa fa-font-awesome')->setAccessLogic(function (){
    return true;
});

