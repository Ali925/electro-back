<?php

use SleepingOwl\Admin\Navigation\Page;

// Default check access logic
// AdminNavigation::setAccessLogic(function(Page $page) {
// 	   return auth()->user()->isSuperAdmin();
// });
//
// AdminNavigation::addPage(\App\User::class)->setTitle('test')->setPages(function(Page $page) {
// 	  $page
//		  ->addPage()
//	  	  ->setTitle('Dashboard')
//		  ->setUrl(route('admin.dashboard'))
//		  ->setPriority(100);
//
//	  $page->addPage(\App\User::class);
// });
//
// // or
//
// AdminSection::addMenuPage(\App\User::class)

return [
    [
            'title' => 'Məhsullar',
        'priority' => 2,
        'icon' => 'fa fa-folder-open',
        'accessLogic' => function(){return true;},
        'pages' => [
            // (new Page(\App\Models::class))
            // ->setTitle('Məhsul növü')
            // ->setIcon('fa fa-paperclip')
            // ->setPriority(10),
            // (new Page(\App\Firms::class))
            // ->setTitle('Brendlər')
            // ->setIcon('fa fa-font-awesome')
            // ->setPriority(10),
            (new Page(\App\Types::class))
            ->setTitle('Model')
            ->setIcon('fa fa-android')
            ->setPriority(10),
            (new Page(\App\Colors::class))
            ->setTitle('Rənglər')
            ->setIcon('fa fa-eyedropper')
            ->setPriority(10),
            (new Page(\App\Products::class))
            ->setTitle('Məhsullar')
            ->setIcon('fa fa-mobile')
            ->setPriority(10),
            (new Page(\App\Reviews::class))
            ->setTitle('Rəylər')
            ->setIcon('fa fa-comments')
            ->setPriority(10)
        ]
    ],
    [
            'title' => 'Blog',
        'priority' => 2,
        'icon' => 'fa fa-folder-open',
        'accessLogic' => function(){return true;},
        'pages' => [
            (new Page(\App\BlogTitles::class))
            ->setTitle('Kategoriyalar')
            ->setIcon('fa fa-bars')
            ->setPriority(10),
            (new Page(\App\BlogTags::class))
            ->setTitle('Etiketlər')
            ->setIcon('fa fa-tags')
            ->setPriority(10),
            (new Page(\App\BlogTexts::class))
            ->setTitle('Yazılar')
            ->setIcon('fa fa-file-text')
            ->setPriority(10),
            (new Page(\App\BlogComments::class))
            ->setTitle('Şərhlər')
            ->setIcon('fa fa-comments')
            ->setPriority(10),
        ]
    ]
];