<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\BlogTexts;
use App\Subscribes;
use Illuminate\Database\Eloquent\Model;

use App\Mail\SubscribeMailler;
use Illuminate\Support\Facades\Mail;

AdminSection::registerModel(BlogTexts::class, function (ModelConfiguration $model){
    $model->setTitle('Yazılar');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('blog_titles.name', 'Kategoriya'),
            AdminColumn::lists('blog_tags.name','Etiketlər'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::select('blog_title_id', 'Kategoriya')
                ->setModelForOptions(new \App\BlogTitles())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name')->required(),
                AdminFormElement::multiselect('blog_tags','Etiketlər')
                ->setModelForOptions(new \App\BlogTags())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name'),
                AdminFormElement::ckeditor('text', 'Mətn')->required(),
                AdminFormElement::image('image', 'Şəkil')->required()

        ]);


        return $form;
    });

    $model->created(function(){

        $emails = Subscribes::all(['email'])->toArray();

        $query = BlogTexts::orderBy('id', 'DESC')->first();

        $mail = Mail::to($emails)->send(new SubscribeMailler($query->name, $query->text, $query->image));

    });
});

