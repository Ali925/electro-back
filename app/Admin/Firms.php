<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Firms;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Firms::class, function (ModelConfiguration $model){
    $model->setTitle('Brendlər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::lists('models.name','Məhsul növü'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required()->unique(),
                AdminFormElement::multiselect('models','Məhsul növü')
                ->setModelForOptions(new \App\Models())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name')

        ]);

        return $form;
    });
})->addMenuPage(Firms::class, 1)->setIcon('fa fa-font-awesome')->setAccessLogic(function (){
    return true;
});
