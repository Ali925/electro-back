<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Types;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Types::class, function (ModelConfiguration $model){
    $model->setTitle('Model');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('firms.name', 'Brend'),
            AdminColumn::text('models.name', 'Məhsul növü')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required()->unique(),
                AdminFormElement::select('firm_id', 'Brend')
                ->setModelForOptions(new \App\Firms())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name'),
                AdminFormElement::select('model_id', 'Məhsul növü')
                ->setModelForOptions(new \App\Models())
                ->setLoadOptionsQueryPreparer(function ($item,$query){
                    return $query->orderBy('name','asc');
                })
                ->setDisplay('name'),

        ]);

        return $form;
    });
});
