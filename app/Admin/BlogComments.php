<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\BlogComments;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(BlogComments::class, function (ModelConfiguration $model){
    $model->setTitle('Şərhlər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('text','Şərh'),
            AdminColumn::relatedLink('customers.name','İstifadəçi adı (qeydə alınmış)'),
            AdminColumn::relatedLink('customers.email','İstifadəçi emaili (qeydə alınmış)'),
            AdminColumn::text('username','İstifadəçi adı (qeydə alınmamış)'),
            AdminColumn::text('useremail','İstifadəçi emaili (qeydə alınmamış)'),
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('text','Şərh')->required(),
        ]);

        return $form;
    });
});

