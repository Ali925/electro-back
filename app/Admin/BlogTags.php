<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\BlogTags;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(BlogTags::class, function (ModelConfiguration $model){
    $model->setTitle('Etiketlər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required()->unique()

        ]);

        return $form;
    });
});

