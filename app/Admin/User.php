<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\User;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(User::class, function (ModelConfiguration $model){
    $model->setTitle('Administratorlar');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('email','Email')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::text('email', 'Email')->required()->unique(),
                AdminFormElement::password('password', 'Password')->required()
        ]);

        return $form;
    });
})->addMenuPage(User::class, 1)->setIcon('fa fa-users')->setAccessLogic(function (){
    return true;
});
