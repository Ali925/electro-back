<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Colors;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Colors::class, function (ModelConfiguration $model){
    $model->setTitle('Rənglər');

    Meta::addJS('jquery', asset('js/jquery-3.1.0.min.js'));
    Meta::addJS('set-colors', asset('js/set-colors.js'));

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('code','Kod')->setHtmlAttribute('class', 'color-code')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::text('code', 'Kod')->required()->unique()
        ]);

        return $form;
    });
});

