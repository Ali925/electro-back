<?php
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Customers;
use Illuminate\Database\Eloquent\Model;

AdminSection::registerModel(Customers::class, function (ModelConfiguration $model){
    $model->setTitle('İstifadəçilər');

    $model->onDisplay(function (){
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::text('name','Name'),
            AdminColumn::text('surname','Surname'),
            AdminColumn::text('email','Email'),
            AdminColumnEditable::checkbox('validated','Təsdiqlənib', 'Təsdiqlənməyib')->setLabel('Təsdiqlənib')
        ]);
        //$display->paginate(10);
        return $display;
    });

    $model->onCreateAndEdit(function (){
        $form = AdminForm::panel()->addBody([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::text('surname', 'Surname')->required(),
                AdminFormElement::text('email', 'Email')->required()->unique(),
                AdminFormElement::password('password', 'Password')->required(),
                AdminFormElement::checkbox('validated', 'Təsdiqlənib')
        ]);

        return $form;
    });
})->addMenuPage(Customers::class, 5)->setIcon('fa fa-users')->setAccessLogic(function (){
    return true;
});
