<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeMailler extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $image;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($titles, $messages, $images)
    {
        $this->title = $titles;
        $this->message = $messages;
        $this->image = $images;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.subscribe')
        ->subject("UNIMAX saytında yenilik əlavə olundu")
        ->with([
            'maintitle' => $this->title,
            'mainmessage' => $this->message,
            'mainimage' => (url('/')."/".$this->image)
        ]);
    }
}
