<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageMailler extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $name;
    public $email;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($names, $titles, $emails, $messages)
    {
        $this->title = $titles;
        $this->email = $emails;
        $this->message = $messages;
        $this->name = $names;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $thisEmail = $this->email;
        $thisTitle = $this->title;

        return $this->view('emails.message')
        ->from($thisEmail)
        ->subject($thisTitle)
        ->with([
            'maintitle' => $this->title,
            'mainmessage' => $this->message,
            'mainemail' => $this->email,
            'mainname' => $this->name
        ]);
    }
}
