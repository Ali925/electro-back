<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{

	public function carts(){
        return $this->hasMany('App\Carts', 'customer_id');
    }

    public function reviews(){
        return $this->hasMany('App\Reviews', 'customer_id');
    }

    public function blog_comments(){
        return $this->hasMany('App\BlogComments', 'customer_id');
    }
}
