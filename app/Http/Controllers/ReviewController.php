<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Reviews;



class ReviewController extends Controller
{
	 // public function getall(Request $request)
  //   {
  //   	$query = Carts::where("customer_id", $request->userid)->with("products")->get();

  //       return response()->json($query);
  //   }

    public function add(Request $request){

	   $query = new Reviews();
     if($request->userid)
	     $query->customer_id = $request->userid;
     if($request->username)
      $query->username = $request->username;
	   $query->product_id = $request->productid;
       if($request->stars > -1)
	       $query->stars = $request->stars;
       if($request->review)
	       $query->review = $request->review;
	   $query->save();

        return response()->json($query);
    }
}