<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\BlogTexts;
use App\BlogTitles;
use App\BlogTags;
use App\BlogComments;


class BlogController extends Controller
{
	 public function getall(Request $request)
    {
    	$query["titles"] = BlogTitles::all();
        $query["posts"] = BlogTexts::where("blog_title_id", $request->pageid)->with("blog_titles")->with("blog_tags")->orderBy('id', 'DESC')->paginate(5); 
        $query["tags"] = BlogTags::all();
        $query["last"] = BlogTexts::select("id", "blog_title_id", "name", "image", "created_at")->orderBy('id', 'DESC')->take(5)->get();

        return response()->json($query);
    }

    public function getone(Request $request)
    {
    	$query["titles"] = BlogTitles::all();
        $query["post"] = BlogTexts::where("id", $request->blogid)->with("blog_titles")->with("blog_tags")->with("blog_comments")->first(); 
        $query["tags"] = BlogTags::all();
        $query["last"] = BlogTexts::select("id", "blog_title_id", "name", "image", "created_at")->orderBy('id', 'DESC')->take(5)->get();
        $query["comments"] = BlogComments::where("blog_id", $query["post"]->id)->with("customers")->get();

        return response()->json($query);
    }

    public function addcomment(Request $request){

	   $query = new BlogComments();
     if($request->userid)
	     $query->customer_id = $request->userid;
     if($request->username)
      $query->username = $request->username;
  	if($request->email)
      $query->useremail = $request->email;
	   $query->blog_id = $request->blogid;
	   $query->text = $request->comment;
	   $query->save();

        return response()->json($query);
    }

    public function search(Request $request){
        if($request->string){
          $query["titles"] = BlogTitles::all();
          $query["posts"] = BlogTexts::where('name','LIKE','%'.$request->string.'%')->orWhere('text','LIKE','%'.$request->string.'%')->with("blog_titles")->with("blog_tags")->orderBy('id', 'DESC')->get(); 
          $query["tags"] = BlogTags::all();
          $query["last"] = BlogTexts::select("id", "blog_title_id", "name", "image", "created_at")->orderBy('id', 'DESC')->take(5)->get();
        } else if($request->tagID){
          $query["titles"] = BlogTitles::all();
          $query["allposts"] = BlogTexts::with("blog_titles")->with("blog_tags")->orderBy('id', 'DESC')->get(); 
          $query["tags"] = BlogTags::all();
          $query["last"] = BlogTexts::select("id", "blog_title_id", "name", "image", "created_at")->orderBy('id', 'DESC')->take(5)->get();
          $query["posts"] = array();
          foreach ($query["allposts"] as $key => $value) {
            foreach($value->blog_tags as $keyt => $valuet){
              if($valuet->id == $request->tagID){
                array_push($query["posts"], $value);
                break;
              }
            }
          }
        }

        return response()->json($query);
    }
}