<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customers;
use App\Carts;

use App\Mail\ActivateMailler;
use App\Mail\LostMailler;
use Illuminate\Support\Facades\Mail;




class UserController extends Controller
{
	 public function signup(Request $request)
    {
    	$query = new Customers();
        $query->name = $request->name;
        $query->surname = $request->surname;
        $query->password = $request->password;
        $query->email = $request->email;
        $query->validated = 0;
        $query->save();


        if($query->id){
            $timestamp = strtotime($query->created_at);
            $url = $request->url . "&id=" . $query->id . "&stamp=" . $timestamp;
            Mail::to($request->email)->send(new ActivateMailler($url));
        }

        return response()->json($query);
    }

    public function signin(Request $request)
    {
        $query = Customers::where("email", $request->email)->where("password", $request->password)->with("carts")->get();

        return response()->json($query);
    }

    public function getuser(Request $request)
    {
        $query['user'] = Customers::where("email", $request->email)->first();
        $query['carts'] = Carts::where("customer_id", $query['user']->id)->with("products")->get();

        return response()->json($query);
    }

    public function active(Request $request)
    {
        $query = Customers::where("id", $request->id)->first();
        if(strtotime($query->created_at) == $request->timestamp){
            $query2 = Customers::where("id", $request->id)->first();
            $query2->validated = 1;
            $query2->save();
            return response()->json(true);
        }
        else
            return response()->json(false);
    }

    public function resend(Request $request)
    {
        $query = Customers::where("email", $request->email)->first();
        $timestamp = strtotime($query->created_at);
        $url = $request->url . "&id=" . $query->id . "&stamp=" . $timestamp;
        $mail = Mail::to($request->email)->send(new ActivateMailler($url));
        return response()->json($mail);
    }

    public function lost(Request $request)
    {
        $query = Customers::where("email", $request->email)->first();

        if($query && $query->id){
            $pass = str_random(8);
            $query->password = $pass;
            $query->save();
            $mail = Mail::to($request->email)->send(new LostMailler($pass));
            return response()->json($mail);
        } else {
            return response()->json("not found");
        }

        
    }
}