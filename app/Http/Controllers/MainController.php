<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Firms;
use App\Types;
use App\Models;
use App\Products;
use App\Carts;
use App\Reviews;
use App\Subscribes;
use App\BlogTitles;
use App\BlogTexts;
use App\Banners;
use DB; 

use App\Mail\MessageMailler;
use Illuminate\Support\Facades\Mail;



class MainController extends Controller
{

    public function getFirstApp(Request $request)
    {
    	$firms = Firms::with('models')->get();
    	$response['allTypes'] = Types::all();
    	$models = Models::all();
    	$response['allBlogTitles'] = BlogTitles::all();
    	foreach ($models as $model) {
			$model["model_count"] = Products::where("model_id", $model["id"])->where("is_show", 1)->count();
		}
		foreach ($firms as $firm) {
			$firm["product_count"] = Products::where("firm_id", $firm["id"])->where("is_show", 1)->count();
		}
		$models[0]["models_count"] = Products::where("is_show", 1)->count();
		$response['allModels'] = $models;
		$response['allFirms'] = $firms;
        return response()->json($response);
    } 


    public function getHome(Request $request){
        $response['lastProducts'] = Products::where("is_show", 1)->orderBy('id', 'DESC')->take(10)->get();
        $response['mostSelected'] = Carts::select('product_id', DB::raw('COUNT(product_id) as count'))->groupBy('product_id')->orderBy('count', 'desc')->take(3)->with("products")->get();
        $response['discountProducts'] = Products::where("is_show", 1)->where("is_discount", 1)->take(3)->get();
        $response['starProducts'] = Reviews::select('product_id', DB::raw('COUNT(product_id) as stars'))->groupBy('product_id')->orderBy('stars', 'desc')->take(3)->with("products")->get();
        $response['smartphones'] = Products::where("is_show", 1)->where("model_id", 1)->take(5)->get();
        $response['mobiles'] = Products::where("is_show", 1)->where("model_id", 2)->take(5)->get();
        $response['tablets'] = Products::where("is_show", 1)->where("model_id", 4)->take(5)->get();
        $response['access'] = Products::where("is_show", 1)->where("model_id", 5)->take(5)->get();
        $response['lastblogs'] = BlogTexts::orderBy('id', 'DESC')->take(3)->get();
        $response['banners'] = Banners::all();

        return response()->json($response);
    }

    public function getFooter(Request $request)
    {
        $response['allModels'] = Models::all();
        $response['allBlogTitles'] = BlogTitles::all();

        return response()->json($response);
    } 
      
    public function sendMessage(Request $request)
    {
        $mail = Mail::to("info@unimax.az")->send(new MessageMailler($request->name, $request->title, $request->email, $request->message));
        return response()->json($mail);
    }

    public function subscribe(Request $request)
    {
        $query = new Subscribes();
        $query->email = $request->email;
        $query->save();

        return response()->json($query);
    }

}
