<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Products;
use App\Reviews;
use App\Firms;
use App\Models;
use App\Types;
use App\Banners;




class ShopController extends Controller
{

    public function getData(Request $request)
    {
        $sort_text = "id:DESC";
        $show_by = $request->show;
        if($request->sort)
            $sort_text = $request->sort;
        $explode_a = explode(":", $sort_text);
        $sort_title = $explode_a[0];
        $sort_type = $explode_a[1];
    	if($request->model_id && $request->model_id != 0){
    		if($request->type_id && $request->type_id != null){
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                else
    			 $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->paginate($show_by);
	    		$response['minPrice'] = Products::where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->min('price');
				$response['minNewPrice'] = Products::where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->min('new_price');
				$response['maxPrice'] = Products::where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->max('price');
				$response['maxNewPrice'] = Products::where("model_id", $request->model_id)->where("type_id", $request->type_id)->where("is_show", 1)->max('new_price');
    		} else {
                if($request->brend_id && $request->brend_id != null){
                    if($sort_title == "price" && $sort_type == "ASC")
    	    		     $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                    else if($sort_title == "price" && $sort_type == "DESC")
                        $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                    else
                        $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->paginate($show_by);
    	    		$response['minPrice'] = Products::where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->min('price');
    				$response['minNewPrice'] = Products::where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->min('new_price');
    				$response['maxPrice'] = Products::where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->max('price');
    				$response['maxNewPrice'] = Products::where("model_id", $request->model_id)->where("firm_id", $request->brend_id)->where("is_show", 1)->max('new_price');
                } else {
                    if($sort_title == "price" && $sort_type == "ASC")
                        $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                    else if($sort_title == "price" && $sort_type == "DESC")
                        $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                    else
                        $response['allProducts'] = Products::with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->paginate($show_by);
                    $response['minPrice'] = Products::where("model_id", $request->model_id)->where("is_show", 1)->min('price');
                    $response['minNewPrice'] = Products::where("model_id", $request->model_id)->where("is_show", 1)->min('new_price');
                    $response['maxPrice'] = Products::where("model_id", $request->model_id)->where("is_show", 1)->max('price');
                    $response['maxNewPrice'] = Products::where("model_id", $request->model_id)->where("is_show", 1)->max('new_price');
                }
			}
    	}
    	else{
            if($sort_title == "price" && $sort_type == "ASC")
    		  $response['allProducts'] = Products::with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
            else if($sort_title == "price" && $sort_type == "DESC")
                $response['allProducts'] = Products::with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
            else
                $response['allProducts'] = Products::with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->paginate($show_by);
			$response['minPrice'] = Products::where("is_show", 1)->min('price');
			$response['minNewPrice'] = Products::where("is_show", 1)->min('new_price');
			$response['maxPrice'] = Products::where("is_show", 1)->max('price');
			$response['maxNewPrice'] = Products::where("is_show", 1)->max('new_price');
		}

        $response['lastProducts'] = Products::where("is_show", 1)->orderBy('id', 'DESC')->take(4)->get();
        $response['banner'] = Banners::where("type", "3")->get();

        return response()->json($response);
    } 

    public function getOneData(Request $request)
    {
    	$response['product'] = Products::where("id", $request->id)->with('colors')->with('reviews')->first();

        $response['related'] = Products::where("type_id", $response['product']->type_id)->with('colors')->with('reviews')->take(4)->get();

        $response['reviews'] = Reviews::where("product_id", $request->id)->with("customers")->get();
        $response['firm_name'] = Firms::find($response['product']->firm_id);

        if($response['product']->type_id != null)
            $response['type_name'] = Types::find($response['product']->type_id);

        $response['accessories'] = Products::where("model_id", 5)->where("firm_id", $response['product']->firm_id)->take(4)->get();

        return response()->json($response);
    } 

    public function getFilterData(Request $request)
    {
        $sort_text = "id:DESC";
        $show_by = $request->show;
        if($request->sort)
            $sort_text = $request->sort;
        $explode_a = explode(":", $sort_text);
        $sort_title = $explode_a[0];
        $sort_type = $explode_a[1];

        if($request->model_id){
            if($request->firm_ids){
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                else
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->paginate($show_by);
            } else {
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                else
                    $response['allProducts'] = Products::where("is_show", 1)->where("model_id", $request->model_id)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->paginate($show_by);
            }
        } else {
            if($request->firm_ids){
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::where("is_show", 1)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::where("is_show", 1)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                else
                    $response['allProducts'] = Products::where("is_show", 1)->whereIn("firm_id", $request->firm_ids)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->paginate($show_by);
            } else {
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::where("is_show", 1)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->paginate($show_by);
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::where("is_show", 1)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->paginate($show_by);
                else
                    $response['allProducts'] = Products::where("is_show", 1)->with("colors")->with('reviews')->where("price", ">=", $request->minPrice)->where("price", "<=", $request->maxPrice)->orderBy($sort_title, $sort_type)->paginate($show_by);
            }
        }

        $response['banner'] = Banners::where("type", "3")->get();

        return response()->json($response);
    }   

public function search(Request $request){
        $sort_text = "id:DESC";
        if($request->sort)
            $sort_text = $request->sort;
        $explode_a = explode(":", $sort_text);
        $sort_title = $explode_a[0];
        $sort_type = $explode_a[1];

        if($request->model_id && $request->model_id != 0){
                if($sort_title == "price" && $sort_type == "ASC")
                    $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->get();
                else if($sort_title == "price" && $sort_type == "DESC")
                    $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->get();
                else
                    $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("model_id", $request->model_id)->where("is_show", 1)->orderBy($sort_title, $sort_type)->get();

                    $response['minPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("model_id", $request->model_id)->where("is_show", 1)->min('price');
                    $response['minNewPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("model_id", $request->model_id)->where("is_show", 1)->min('new_price');
                    $response['maxPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("model_id", $request->model_id)->where("is_show", 1)->max('price');
                    $response['maxNewPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("model_id", $request->model_id)->where("is_show", 1)->max('new_price');
        }
        else{
            if($sort_title == "price" && $sort_type == "ASC")
                $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'DESC')->get();
            else if($sort_title == "price" && $sort_type == "DESC")
                $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->orderBy(\DB::raw('-`new_price`'), 'ASC')->get();
            else
                $response['allProducts'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->with('colors')->with('reviews')->where("is_show", 1)->orderBy($sort_title, $sort_type)->get();
            $response['minPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("is_show", 1)->min('price');
            $response['minNewPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("is_show", 1)->min('new_price');
            $response['maxPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("is_show", 1)->max('price');
            $response['maxNewPrice'] = Products::where('name','LIKE','%'.$request->string.'%')
                ->where("is_show", 1)->max('new_price');
        }

        $response['lastProducts'] = Products::where("is_show", 1)->orderBy('id', 'DESC')->take(4)->get();
        $response['banner'] = Banners::where("type", "3")->get();

        return response()->json($response);
      }

}
