<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Carts;
use App\Products;


class CartController extends Controller
{
	 public function getall(Request $request)
    {
    	$query["cart"] = Carts::where("customer_id", $request->userid)->with("products")->get();
        $query["colors"] = array();
        foreach ($query["cart"] as $key => $value) {
            $color = Products::where("id", $value->products->id)->with("colors")->first();
            array_push($query["colors"], $color); 
        }

        return response()->json($query);
    }

    public function remove(Request $request){
    	$query = Carts::where("id", $request->itemid)->delete();

        return response()->json($query);
    }

    public function add(Request $request){

    	$res = Carts::where("customer_id", $request->userid)->where("product_id", $request->productid)->first();

    	if(is_object($res) && $res->count && $res->color_id == $request->color_id){
    		$query = Carts::where("customer_id", $request->userid)->where("product_id", $request->productid)->first();
	        $query->total = intval($query->total) + intval($request->total);
	        $query->count = intval($query->count) + intval($request->count);
	        $query->save();	
    	} else {
	    	$query = new Carts();
	        $query->customer_id = $request->userid;
	        $query->product_id = $request->productid;
	        $query->count = $request->count;
	        $query->total = $request->total;
            if($request->color_id)
                $query->color_id = $request->color_id;
	        $query->save();
    	}

        return response()->json($query);
    }

    public function addRemove(Request $request){
        if($request->type == "add"){
            $query = Carts::where("id", $request->id)->first();
            $query->total = intval($query->total) + intval($query->total)/intval($query->count);
            $query->count = intval($query->count) + 1;
            $query->save(); 

            return response()->json($query);
        } else {
            $query = Carts::where("id", $request->id)->first();
            $query->total = intval($query->total) - intval($query->total)/intval($query->count);
            $query->count = intval($query->count) - 1;
            $query->save();

            return response()->json($query);
        }
    }

    public function changeColor(Request $request){
            $query = Carts::where("id", $request->id)->first();
            $query->color_id = $request->color_id;
            $query->save(); 

            return response()->json($query);
    }

}