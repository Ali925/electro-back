<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTitles extends Model
{

	public function blog_texts(){
        return $this->hasMany('App\BlogTexts', 'blog_title_id');
    }

}
