<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('blog_title_id');
            $table->string('name');
            $table->text('text');
            $table->string('image');
            $table->timestamps();
            
            $table->foreign('blog_title_id')->references('id')->on('blog_titles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_texts');
    }
}
