<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('firm_id');
            $table->unsignedInteger('model_id')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->boolean('is_stock');
            $table->boolean('is_show');
            $table->string('network')->nullable();
            $table->string('twog')->nullable();
            $table->string('threeg')->nullable();
            $table->string('fourg')->nullable();
            $table->string('date')->nullable();
            $table->string('network_speed')->nullable();
            $table->string('size')->nullable();
            $table->string('weight')->nullable();
            $table->string('sims')->nullable();
            $table->string('display_type')->nullable();
            $table->string('display_inch')->nullable();
            $table->string('display_pixel')->nullable();
            $table->string('is_multitouch')->nullable();
            $table->string('sensor')->nullable();
            $table->string('display_defender')->nullable();
            $table->string('os')->nullable();
            $table->string('chipset')->nullable();
            $table->string('cpu')->nullable();
            $table->string('gpu')->nullable();
            $table->string('memory')->nullable();
            $table->string('ram')->nullable();
            $table->string('memory_card')->nullable();
            $table->string('main_camera_pixel')->nullable();
            $table->string('main_camera_desc')->nullable();
            $table->string('video')->nullable();
            $table->string('second_camera')->nullable();
            $table->string('audio')->nullable();
            $table->string('vibration')->nullable();
            $table->string('fm')->nullable();
            $table->string('wlan')->nullable();
            $table->string('bluetooth')->nullable();
            $table->string('gps')->nullable();
            $table->string('usb')->nullable();
            $table->string('messaging')->nullable();
            $table->string('batery_size')->nullable();
            $table->string('batery_first_length')->nullable();
            $table->string('batery_second_length')->nullable();
            $table->string('other_features')->nullable();
            $table->text('images')->nullable();
            $table->integer('price');
            $table->text('desc_text')->nullable();
            $table->text('desc_images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
