<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function (){

    Route::get('firstApp', 'MainController@getFirstApp');
    Route::get('startHome', 'MainController@getHome');
    Route::get('getFooter', 'MainController@getFooter');

    Route::post('getProducts', 'ShopController@getData');

    Route::post('getProduct', 'ShopController@getOneData');

    Route::post('filterProduct', 'ShopController@getFilterData');

    Route::post('signup', 'UserController@signup');

    Route::post('signin', 'UserController@signin');

    Route::post('getcart', 'CartController@getall');

    Route::post('getuser', 'UserController@getuser');

    Route::post('removecart', 'CartController@remove');

    Route::post('addcart', 'CartController@add');

    Route::post('addRemoveCart', 'CartController@addRemove');

    Route::post('changeCartColor', 'CartController@changeColor');

    Route::post('addreview', 'ReviewController@add');

    Route::post('addcomment', 'BlogController@addcomment');

    Route::post('getblogs', 'BlogController@getall');

    Route::post('getblog', 'BlogController@getone');

    Route::post('searchprod', 'ShopController@search');

    Route::post('searchblog', 'BlogController@search');

    Route::post('activate', 'UserController@active');
    Route::post('resendLink', 'UserController@resend');
    Route::post('lostPass', 'UserController@lost');

    Route::post('sendMessage', 'MainController@sendMessage');
    Route::post('subscribe', 'MainController@subscribe');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

