$( document ).ready(function() {
	$("tbody>tr>td.row-text.color-code").each(function(){
		$(this).css("background-color", $(this).find("div.color-code").text());
	});
	$("tbody>tr>td.row-text:not(.color-code)").each(function(){
		$(this).css("background-color", $(this).next().find("div.color-code").text());
	});
});