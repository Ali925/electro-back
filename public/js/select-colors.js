$( document ).ready(function() {
	$("label[for='colors[]']").next().find("li.multiselect__element").each(function(index){
		$(this).css("background-color", $(this).children().children().text());
	});

	$("label[for='colors[]']").next().find("span.multiselect__tag").each(function(){
			$(this).css("background-color", $(this).children().text());
	});

	$("td.row-lists").find("span.label").each(function(){
		$(this)[0].style.setProperty("background-color", $(this).text(), 'important');
	});

	$("label[for='colors[]']").next().find("select#colors").change(function(){
		$("label[for='colors[]']").next().find("span.multiselect__tag").each(function(){
			$(this).css("background-color", $(this).children().text());
		});
	});
});